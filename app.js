const sqlite3 = require('sqlite3').verbose();
const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');

const db = new sqlite3.Database('./police.db');


const app = express();
app.use(bodyParser.json({ type: 'application/json' }));

app.get('/:state/:city', function(req, res) { 
    db.each(`SELECT * FROM DEPARTMENTS WHERE STATE='${req.params.state}' AND CITY='${req.params.city}'`,
function(err, rows) {
    res.json(rows);
});
});

app.get('/:state', function(req, res) { 
    const pushed = [];
    db.each(`SELECT * FROM DEPARTMENTS WHERE STATE='${req.params.state}'`,
function(err, rows) {
    pushed.push(rows);
});
setTimeout(()=> {
res.json(pushed);
}, 200);
});

http.createServer(app).listen(8020, () => {
  console.log('Express server listening on port  8020');
});